// Typewriter JS
const hero = document.getElementById('hero');

const typewriter = new Typewriter(hero, {
  loop: true,
  delay: 50,
  deleteSpeed: 50,
});

typewriter
  .pauseFor(1000)
  .typeString("Hi!")
  .pauseFor(500)
  .deleteAll(1)
  .typeString("I'm Mikee,")
  .pauseFor(500)
  .deleteAll(1)
  .typeString("and I'm a Full Stack Developer")
  .pauseFor(1000)
  .deleteAll(1)
  .start();

// Navbar Fixed Top
$(function () {
  $(document).scroll(function () {
    var $nav = $(".fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    $nav.toggleClass('navbar-light', $(this).scrollTop() > $nav.height());
    $nav.toggleClass('navbar-dark', $(this).scrollTop() < $nav.height());
  });
});

// Initialize AOS
AOS.init();

// Initialize Match Height
// Match card height
function MatchHeight() {
  $('.card-img-top').matchHeight({});
  $('.card-body').matchHeight({});
  $('.card-title').matchHeight({});
}

// Function that runs when all HTML is loaded
$(document).ready(function() {
  MatchHeight();
});